/**
 *	Home page javascript codes
 *
 *	@author rjosephporter
*/
var search_results;
var product_info_big = $('#product-info-big').html();

$('#input-search').keypress(function(e){
	if(e.which == 13) {
    	search_event($(this).val());
    }
});

$('#button-search').click(function(){
	search_event($('#input-search').val());
});

function search_event(search_query) {
	$('.error-msg').hide();
	$('#product-info-results-small').hide();
	$('#product-info-big').html('');
	$('#search-results').show();

	$('#search-results-body').spin();

	if (search_query.trim() == '') {
		$('#search-results-body').spin(false);
		$('.error-msg').show();
		return;
	}

	$.ajax({
		url: 'debug/index',
		dataType: 'json',
		type: 'POST',
		data: { search : search_query },
		success: function(data, status){
			if(status == 'success' && !$.isEmptyObject(data)) {
				search_results = data;
				$.ajax({
					url: 'home/product_info_small',
					type: 'POST',
					data: { products : data },
					dataType: 'html',
					success: function(data2, status2) {
						if(status2 == 'success') {
							$('#search-results-body').spin(false);
							$('#product-info-results-small').html(data2);
							$('#product-info-results-small').show();
						} else {
							$('#search-results-body').spin(false);
							$('.error-msg').show();
						}
					}
				});			
			} else {
				$('#search-results-body').spin(false);
				$('.error-msg').show();
			}
		}
	});
}

$(document).on('click', '.btn-get-product-offers', function() {
	$('#product-info-big').html('');
	$('#product-info-big').spin();
	var ASIN = $(this).attr('asin');

	$.ajax({
		url: 'home/product_info_big/' + ASIN,
		type: 'POST',
		data: { asin : ASIN, detail : search_results[ASIN] },
		dataType: 'html',
		success: function(data, status) {
			if(status == 'success') {
				$('#product-info-big').spin(false);
				$('#product-info-big').html(data);
			}
		}
	});
	/*
	setTimeout(
		function() 
		{
			$('#product-info-big').spin(false);
			$('#product-info-big').html(product_info_big);
		}, 1000
	);
	*/
});

$('#product-info-big').sticky({
	topSpacing:0,
	getWidthFrom: '#product-info-big-container'
});