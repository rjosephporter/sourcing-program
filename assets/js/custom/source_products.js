/**
 *  Source Products page javascript codes
 *
 *  @author rjosephporter
*/

$(document).ready( function () {

    $('#search-results').hide();
    $('#search-results-body > .row').hide();

    $('.table').dataTable( {
        "bLengthChange": false,
        "bFilter": false,
        "aoColumnDefs": [
            { 'bSortable': false, 'aTargets': [ -1 ] }
        ],
        "bPaginate": false
    } );

    $('.dataTables_wrapper').addClass('table-responsive-wide');


    $('#input-search').keypress(function(e){
        if(e.which == 13) {
            search_event($(this).val());
        }
    });

    $('#button-search').click(function(){
        search_event($('#input-search').val());
    });

    function search_event(search_query) {
        $('#search-results-body > .row').hide();
        $('#search-results').show();
        $('#search-results-body').spin();

        setTimeout(
            function() 
            {
                $('#search-results-body').spin(false);

                if(search_query.trim() == '') {
                    $('#search-results-body > .error-msg').show();
                } else {
                    $('#search-results-body > .row:not(.error-msg)').show();
                }
            }, 3000
        );
    }

} );