<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Model for interfacing with Amazon's MWS API
 *
 * @author 	rjosephporter
 *
 */
class Amazon_model extends CI_Model {

    private $_merchant_id		= 'A1C35S0O3JZKZK';
    private $_aws_access_key	= 'AKIAINCJM7SSQEGQSH4Q';
    private $_marketplace_id	= 'ATVPDKIKX0DER';
    private $_secret_key		= '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';
    private $_version			= '2011-10-01';
    private $_signature_version	= 2;
    private $_signature_method	= 'HmacSHA256';
    private $_service_url		= 'https://mws.amazonservices.com/Products/2011-10-01';

    private $_parameters		= array();
    private $_xml_raw_response;


    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();

        // Initialize constant parameters for API request
        $this->_parameters['AWSAccessKeyId']    = $this->_aws_access_key;
        $this->_parameters['SellerId']          = $this->_merchant_id;
        $this->_parameters['MarketplaceId']     = $this->_marketplace_id;
        $this->_parameters['Version']           = $this->_version;
        $this->_parameters['SignatureMethod']   = $this->_signature_method;
        $this->_parameters['SignatureVersion']  = $this->_signature_version;
    }

    private function _curlRequest($parameters)
    {
        $query = http_build_query($parameters, '', '&');

        $allHeadersStr = array(
            "Content-Type: application/x-www-form-urlencoded; charset=utf-8"
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->_service_url);
        curl_setopt($ch, CURLOPT_PORT, 443);
        curl_setopt($ch, CURLOPT_USERAGENT, "Sourcing Program");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $allHeadersStr);
        curl_setopt($ch, CURLOPT_HEADER, true); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $response = "";
        $response = curl_exec($ch);

        if($response === false) {
            $exProps["Message"] = curl_error($ch);
            $result = $exProps["Message"];
        } else {
            list($other, $responseBody) = explode("\r\n\r\n", $response, 2);
            $response = new SimpleXMLElement($responseBody);
            $this->_xml_raw_response = $response;
        }

        curl_close($ch);

        return $response;
    }

    public function listMatchingProducts($query, $queryContextId = false)
    {    	
        $parameters = $this->_parameters;
    	$parameters['Action']		= 'ListMatchingProducts';
        $parameters['Timestamp']    = $this->_getFormattedTimestamp();
    	$parameters['Query']		= $query;
        
        if($queryContextId !== false)
            $parameters['QueryContextId'] = $queryContextId;

    	$parameters['Signature']	= $this->_signParameters($parameters, $this->_secret_key);

        return $this->_curlRequest($parameters);
    }

    public function getMatchingProduct($asin_array)
    {
        $parameters = $this->_parameters;
        $parameters['Action']       = 'GetMatchingProduct';
        $parameters['Timestamp']    = $this->_getFormattedTimestamp();
        
        $index = 1;
        foreach($asin_array as $asin) {
            $parameters['ASINList.ASIN.'.$index] = $asin;
            $index++;
        }

        $parameters['Signature'] = $this->_signParameters($parameters, $this->_secret_key);

        return $this->_curlRequest($parameters);
    }

    public function getCompetitivePricingForASIN($asin_array)
    {
        $parameters = $this->_parameters;
        $parameters['Action']    =   'GetCompetitivePricingForASIN';
        $parameters['Timestamp'] =   $this->_getFormattedTimestamp();

        $index = 1;
        foreach($asin_array as $asin) {
            $parameters['ASINList.ASIN.'.$index] = $asin;
            $index++;
        }

        $parameters['Signature'] = $this->_signParameters($parameters, $this->_secret_key);

        return $this->_curlRequest($parameters);
    }

    public function getLowestOfferListingsForASIN($asin_array)
    {
        $parameters = $this->_parameters;
        $parameters['Action']       = 'GetLowestOfferListingsForASIN';
        $parameters['Timestamp']    = $this->_getFormattedTimestamp();

        $index = 1;
        foreach($asin_array as $asin) {
            $parameters['ASINList.ASIN.'.$index] = $asin;
            $index++;
        }

        $parameters['Signature'] = $this->_signParameters($parameters, $this->_secret_key);

        return $this->_curlRequest($parameters);
    }

    public function set($variable, $value)
    {
    	if(isset($this->$variable))
    		$this->$variable = $value; 
    }

    public function get($variable)
    {
        if(isset($this->$variable))
            return $this->$variable;
    }

    private function _signParameters(array $parameters, $key) {
        //$signatureVersion = $parameters['SignatureVersion'];
        $signatureVersion = $this->_signature_version;
        $algorithm = "HmacSHA1";
        $stringToSign = null;
        if (2 === $signatureVersion) {
            //$algorithm = $this->_config['SignatureMethod'];
            $algorithm = $this->_signature_method;
            //$parameters['SignatureMethod'] = $algorithm;
            $stringToSign = $this->_calculateStringToSignV2($parameters);
        } else {
            throw new Exception("Invalid Signature Version specified");
        }
        return $this->_sign($stringToSign, $key, $algorithm);
    }

    /**
     * Calculate String to Sign for SignatureVersion 2
     * @param array $parameters request parameters
     * @return String to Sign
     */
    private function _calculateStringToSignV2(array $parameters) {
        $data = 'POST';
        $data .= "\n";
        $endpoint = parse_url ($this->_service_url);
        $data .= $endpoint['host'];
        $data .= "\n";
        $uri = array_key_exists('path', $endpoint) ? $endpoint['path'] : null;
        if (!isset ($uri)) {
            $uri = "/";
        }
        $uriencoded = implode("/", array_map(array($this, "_urlencode"), explode("/", $uri)));
        $data .= $uriencoded;
        $data .= "\n";
        uksort($parameters, 'strcmp');
        $data .= $this->_getParametersAsString($parameters);
        return $data;
    }  

    private function _urlencode($value) {
        return str_replace('%7E', '~', rawurlencode($value));
    }

    /**
     * Convert paremeters to Url encoded query string
     */
    private function _getParametersAsString(array $parameters)
    {
        $queryParameters = array();
        foreach ($parameters as $key => $value) {
            $queryParameters[] = $key . '=' . $this->_urlencode($value);
        }
        return implode('&', $queryParameters);
    }  

    /**
     * Computes RFC 2104-compliant HMAC signature.
     */
    private function _sign($data, $key, $algorithm)
    {
        if ($algorithm === 'HmacSHA1') {
            $hash = 'sha1';
        } else if ($algorithm === 'HmacSHA256') {
            $hash = 'sha256';
        } else {
            throw new Exception ("Non-supported signing method specified");
        }
        return base64_encode(
            hash_hmac($hash, $data, $key, true)
        );
    }

    /**
     * Formats date as ISO 8601 timestamp
     */
    private function _getFormattedTimestamp()
    {
        return gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time());
    }

    /**
     * Get Product Size Group and Tier
     *
     * @param   decimal   longest side
     * @param   decimal   median side
     * @param   decimal   shortest side
     * @param   decimal   Length + Girth
     * @param   decimal   weight
     * @return  array     [group, product_size_tier]
     */
    public function getProductSizeTier($longest_side, $median_side, $shortest_side, $length_girth, $weight)
    {

        if($weight == 'N/A') $weight = 0;

        $this->db->select('group, product_size_tier');
        $this->db->from('product_size_tier');

        $where = "(longest_side >= {$longest_side} or {$longest_side} = 0)
                    and (median_side >= {$median_side} or {$median_side} = 0) 
                    and (shortest_side >= {$shortest_side} or {$shortest_side} = 0)
                    and (length_girth >= {$length_girth} or {$length_girth} = 0)
                    and (weight >= {$weight} or {$weight} = 0)";
        $this->db->where($where);
        $this->db->order_by('rank', 'asc'); 
        $this->db->limit(1);

        $query = $this->db->get();

        return $query->row_array();
    }

    /**
     * Get Product referral fee
     *
     * @param   decimal     product's list price
     * @param   string      product group
     * @return  array       [percentage, fee]
     */
    public function getProductReferralFee($price, $product_group)
    {
        $this->db->select('percentage');
        $this->db->from('referral_fee');
        $this->db->where('product_group', $product_group);
        $this->db->order_by('percentage', 'desc');
        $this->db->limit(1);
        $query = $this->db->get();

        $percentage = $query->row_array();
        if(count($percentage) == 0) $percentage = 15;
        else                        $percentage = $percentage['percentage'];

        $fee = $price * ( $percentage / 100 );

        return array('percentage' => $percentage.'%', 'fee' => $fee);

    }

    /**
     * Get Product variable closing fee
     *
     * @param   string      product group
     * @return  decimal     fee
     */
    public function getProductVariableClosingFee($product_group)
    {
        $this->db->select('fee');
        $this->db->from('variable_closing_fee');
        $this->db->where('product_group', $product_group);
        $this->db->order_by('fee', 'desc');
        $this->db->limit(1);
        $query = $this->db->get();

        $result = $query->row_array();
        return $result['fee'];
    }

    /**
     * Get Product category type (Media or Non-Media)
     *
     * @param   string      product group (e.g. Book, DVD, Music)
     * @return  string      category type
     */
    public function getProductCategoryType($product_group)
    {
        $this->db->select('category_type');
        $this->db->from('category_type');
        $this->db->where('product_group', $product_group);
        $this->db->limit(1);
        $query = $this->db->get();

        $result = $query->row_array();

        if(count($result) == 0) $result = 'Non-Media';
        else                    $result = $result['category_type'];

        return result;
    }

}