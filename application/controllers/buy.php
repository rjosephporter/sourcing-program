<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *	Source Products page controller
 *
 *	@author 	rjosephporter
 */
class Buy extends CI_Controller {

	public function index()
	{
		$this->load->helper('html');

		$data['title'] 			= 'Buy';
		$data['main_segment']	= $this->uri->segment(1);
		$data['main_content'] 	= $this->load->view('buy/main_content', '', true);
		$data['additional_css']	= css(array(
												'jquery.dataTables.css',
												'jquery.dataTables_themeroller.css',
												'dataTables.bootstrap.css'
											)
								);
		$data['additional_js'] 	= js(array(
											'jquery.dataTables.js',
											'dataTables.bootstrap.js',
											'custom/source_products.js'
										  )
								);

		$this->load->view('_template/base', $data);
	}

}