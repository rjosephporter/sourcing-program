<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *	Home page controller
 *
 *	@author 	rjosephporter
 */
class Home extends CI_Controller {

	public function index()
	{
		//$this->load->helper('simple_html_dom');
		$this->load->helper('html');
		
		$data['title'] 			= 'Home';
		$data['main_segment']	= $this->uri->segment(1);
		$data['main_content']	= $this->load->view('home/main_content', '', true);
		$data['additional_js']  = js(array(
									'jquery.sticky.js',
									'custom/home.js'));

		$this->load->view('_template/base', $data);
	}

	public function product_info_small()
	{
		$products = $this->input->post('products');
		$data['products'] = $products;
		$this->load->view('home/product_info_small', $data);
	}

	public function product_info_big($asin)
	{
		$data['asin'] = $this->input->post('asin');
		$data['detail'] = $this->input->post('detail');
		$data['detail']['Image_URL'] = str_replace('SL75', 'SL250', $data['detail']['Image_URL']);

		$data['fulfillment_fba']['new'] = array();
		$data['fulfillment_merchant']['new'] = array();

		if(isset($data['detail']['Fulfillment'])) {
			foreach($data['detail']['Fulfillment'] as $key => $fulfillment) {
				if($fulfillment['FulfillmentChannel'] == 'Amazon')
					$data['fulfillment_fba']['new'][] = $fulfillment['ListingPrice'];
				else
					$data['fulfillment_merchant']['new'][] = $fulfillment['ListingPrice'];
			}
		}

		$this->load->view('home/product_info_big', $data);
	}

}