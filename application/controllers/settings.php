<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *	Settings page controller
 *
 *	@author 	rjosephporter
 */
class Settings extends CI_Controller {

	public function index()
	{
		$this->load->helper('html');

		$data['title'] 			= 'Settings';
		$data['main_segment']	= $this->uri->segment(1);
		$data['main_content'] 	= $this->load->view('settings/main_content', '', true);
		$data['additional_css']	= css(array(
												'colorpicker.css'
											)
								);
		$data['additional_js'] 	= js(array(
											'colorpicker.js',
											'custom/settings.js'
										  )
								);

		$this->load->view('_template/base', $data);
	}

}