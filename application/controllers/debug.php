<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *	Debugging controller
 *
 *	@author 	rjosephporter
 */
class Debug extends CI_Controller {

	public function index()
	{
		$this->load->model('api/amazon_model', 'Amazon');

        $search_results = array();

        $search = $this->input->post('search');

		$xml = $this->Amazon->listMatchingProducts($search);
        $xml->registerXPathNamespace('ns2','http://mws.amazonservices.com/schema/Products/2011-10-01/default.xsd');
        $object = $xml->ListMatchingProductsResult->Products;

        $index = 1;

        foreach($object->Product as $info) {
            $ASIN = (string) $info->Identifiers->MarketplaceASIN->ASIN;
            
            $title = $info->AttributeSets->xpath('ns2:ItemAttributes/ns2:Title');
            $search_results[$ASIN]['Title'] = (string) $title[0];
            
            $image_url = $info->AttributeSets->xpath('ns2:ItemAttributes/ns2:SmallImage/ns2:URL');
            $search_results[$ASIN]['Image_URL'] = (string) $image_url[0];

            $category = $info->AttributeSets->xpath('ns2:ItemAttributes/ns2:ProductGroup');
            $search_results[$ASIN]['Category'] = (string) $category[0];

            $weight = $info->AttributeSets->xpath('ns2:ItemAttributes/ns2:PackageDimensions/ns2:Weight');

            if(isset($weight[0])) {
                $weight = json_decode(json_encode($weight[0]), true);
                $weight = $weight[0];
            } else $weight = 'N/A';

            $search_results[$ASIN]['Weight'] = $weight;

            /* Unit Weight */
            $unit_weight = $info->AttributeSets->xpath('ns2:ItemAttributes/ns2:ItemDimensions/ns2:Weight');

            if(isset($unit_weight[0])) {
                $unit_weight = json_decode(json_encode($unit_weight[0]), true);
                $unit_weight = $unit_weight[0];
            } else $unit_weight = 'N/A';

            $search_results[$ASIN]['Unit_weight'] = $unit_weight;
            /* /Unit Weight */

            $list_price_amount = $info->AttributeSets->xpath('ns2:ItemAttributes/ns2:ListPrice/ns2:Amount');
            $list_price_currency = $info->AttributeSets->xpath('ns2:ItemAttributes/ns2:ListPrice/ns2:CurrencyCode');

            if(isset($list_price_amount[0]) && isset($list_price_currency[0]))
                 $list_price = $list_price_amount[0] . " " . $list_price_currency[0];
            else $list_price = 'N/A';

            $search_results[$ASIN]['List_Price'] = $list_price;

            $rank_array = array();

            foreach($info->SalesRankings->children() as $rank) {
                $rank_array[] = (int) $rank->Rank;
            }

            /* Added for FBA Calculation - 03.01.2014 */
            $package_dimensions = array('Height', 'Length', 'Width');
            $search_results[$ASIN]['PackageDimensions'] = array();
            foreach($package_dimensions as $dimension) {
                $package[$dimension] = $info->AttributeSets->xpath('ns2:ItemAttributes/ns2:PackageDimensions/ns2:'.$dimension);
                if(isset($package[$dimension][0])) {
                    $package[$dimension] = json_decode(json_encode($package[$dimension][0]), true);
                    $package[$dimension] = $package[$dimension][0];  

                    $search_results[$ASIN]['PackageDimensions'][$dimension]  = $package[$dimension];
                }              
            }

            $search_results[$ASIN]['SizeGroup'] = 'N/A';
            $search_results[$ASIN]['SizeTier'] = 'N/A';

            if(count($search_results[$ASIN]['PackageDimensions']) > 0) {
                $final_package_dimensions = $search_results[$ASIN]['PackageDimensions'];
                rsort($final_package_dimensions);
                $search_results[$ASIN]['SortedDimensions'] = $final_package_dimensions;

                $length_girth = $search_results[$ASIN]['PackageDimensions']['Length'] + ( ( $search_results[$ASIN]['PackageDimensions']['Height'] * 2 ) + ( $search_results[$ASIN]['PackageDimensions']['Width'] * 2 ) );

                $size_tier = $this->Amazon->getProductSizeTier(
                                                    $final_package_dimensions[0],
                                                    $final_package_dimensions[1],
                                                    $final_package_dimensions[2],
                                                    $length_girth,
                                                    $search_results[$ASIN]['Weight']
                                                );

                $search_results[$ASIN]['SizeGroup'] = $size_tier['group'];
                $search_results[$ASIN]['SizeTier'] = $size_tier['product_size_tier'];
            }

            /* /Added for FBA Calculation */

            //$search_results[$ASIN]['Salesrank'] = (count($rank_array) > 0) ?  min($rank_array) : 'N/A';

            /*Changed to get sales rank from top category (not sub) */
            $search_results[$ASIN]['Salesrank'] = (count($rank_array) > 0) ?  $rank_array[0] : 'N/A';

            /* Try to get parent product's sales rank if current is N/A */
            if($search_results[$ASIN]['Salesrank'] == 'N/A' && isset($info->Relationships->VariationParent->Identifiers->MarketplaceASIN->ASIN)) {
                $parent_ASIN = (string) $info->Relationships->VariationParent->Identifiers->MarketplaceASIN->ASIN;
                $parent_xml = $this->_getMatchingProduct(array($parent_ASIN));
                $parent_salesranks = array();
                foreach($parent_xml->GetMatchingProductResult->Product->SalesRankings->children() as $child) {
                    $parent_salesranks[] = (int) $child->Rank;
                }
                $search_results[$ASIN]['Salesrank'] = (count($parent_salesranks) > 0) ?  $parent_salesranks[0] : 'N/A';
            }

            $index++;

        }

        $asin_array = array_keys($search_results);

        $xml = $this->Amazon->getCompetitivePricingForASIN($asin_array);
        $xml->registerXPathNamespace('ns2','http://mws.amazonservices.com/schema/Products/2011-10-01/default.xsd');
        
        $total_offers = array();
        foreach($xml->GetCompetitivePricingForASINResult as $child) {
            $asin = (string) $child->Product->Identifiers->MarketplaceASIN->ASIN;
            $offer_listings = $child->Product->CompetitivePricing->NumberOfOfferListings;
            $offer_listings_array = json_decode(json_encode($offer_listings));
            $offer_listings_array = (isset($offer_listings_array->OfferListingCount)) ? array_map('intval', $offer_listings_array->OfferListingCount) : array(0);
            $total_offers[$asin] = max($offer_listings_array);      
        }

        foreach($total_offers as $asin => $count) {
            $search_results[$asin]['Total_Offers'] = $count;
        }

        $offer_listings = $this->_lowestOfferListings($asin_array);

        $lowest_offer_array = array();
        foreach($offer_listings as $asin => $details) {
            $search_results[$asin]['Fulfillment'] = $details;
            foreach($details as $key => $detail) {                
                $lowest_offer_array[$asin][] = floatval($detail['ListingPrice']);
            }            
            $lowest_offer = number_format( min($lowest_offer_array[$asin]) , 2 );
            $lowest_offer_index = array_keys($lowest_offer_array[$asin], min($lowest_offer_array[$asin]));
            $lowest_offer_currency = $details[$lowest_offer_index[0]]['ListingPriceCurrency'];
            $search_results[$asin]['Lowest_Offer'] = $lowest_offer . " " . $lowest_offer_currency;
        }
        
        echo json_encode($search_results);        
	}

    private function _lowestOfferListings($asin_array)
    {
        $this->load->model('api/amazon_model', 'Amazon');

        $xml = $this->Amazon->getLowestOfferListingsForASIN($asin_array);
        $xml->registerXPathNamespace('ns2','http://mws.amazonservices.com/schema/Products/2011-10-01/default.xsd');

        $offer_listings = array();
        foreach($xml->GetLowestOfferListingsForASINResult as $child) {
            $asin = (string) $child->Product->Identifiers->MarketplaceASIN->ASIN;
            $index = 1;
            foreach($child->Product->LowestOfferListings->LowestOfferListing as $listing_detail) {
                $fulfillment_channel = (string) $listing_detail->Qualifiers->FulfillmentChannel;
                $listing_price_amount = (string) $listing_detail->Price->ListingPrice->Amount;
                $listing_price_currency = (string) $listing_detail->Price->ListingPrice->CurrencyCode;

                $offer_listings[$asin][] = array(
                                            'FulfillmentChannel'    => $fulfillment_channel,
                                            'ListingPrice'          => $listing_price_amount . " " . $listing_price_currency,
                                            'ListingPriceCurrency'  => $listing_price_currency
                                        );

                $index++;
            }
        }

        return ($offer_listings);

    }

    private function _getMatchingProduct($asin_array)
    {
        $this->load->model('api/amazon_model', 'Amazon');

        $xml = $this->Amazon->getMatchingProduct($asin_array);
        $xml->registerXPathNamespace('ns2','http://mws.amazonservices.com/schema/Products/2011-10-01/default.xsd');

        return $xml;
    }

}