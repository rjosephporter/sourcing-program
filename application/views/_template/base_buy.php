<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.css" rel="stylesheet">

    <!-- DataTables Stylesheet -->
    <link href="<?php echo base_url() ?>assets/css/jquery.dataTables.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/jquery.dataTables_themeroller.css" rel="stylesheet">

    <!-- Main Stylesheet -->
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="navbar navbar-default navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Sourcing Program</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="<?php echo base_url() ?>">Scout</a></li>
            <li><a href="<?php echo base_url('source_products') ?>">Source Products</a></li>
            <li class="active"><a href="<?php echo base_url('buy') ?>">Buy</a></li>
            <li><a href="<?php echo base_url('settings') ?>">Settings</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <!--
            <li><a href="../navbar/">Default</a></li>
            <li class="active"><a href="./">Static top</a></li>
            <li><a href="../navbar-fixed-top/">Fixed top</a></li>
            -->
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="input-group">
                    <input type="search" class="form-control input-lg" placeholder="Search product by name or ASIN">
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="button">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                </div><!-- /input-group -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 source-product-results">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>ASIN</th>
                            <th>Source Link</th>
                            <th>Source Inventory</th>
                            <th>Best Seller Rank</th>
                            <th>Category</th>
                            <th>Store Price</th>
                            <th>Amazon Price</th>
                            <th>Net Profit</th>
                            <th>ROI</th>
                            <th>Number of Sellers</th>
                            <th>Last Ordered</th>
                            <th>Purchased</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><a href="#">EB Brands Remote Controlled Ex ...</a></td>
                            <td><a href="#">B00EH8VDWK</a></td>
                            <td><a href="#">http://www.walmart.com/ip/Remo ...</a></td>
                            <td>5</td>
                            <td>234567</td>
                            <td>Toys</td>
                            <td>$69.88</td>
                            <td>$109.93</td>
                            <td>$23.56</td>
                            <td>25.21%</td>
                            <td>35</td>
                            <td>12/31/2013</td>
                            <td><input type="checkbox"></td>
                        </tr>
                        <tr>
                            <td><a href="#">Pink Whisper Ride Buggy ...</a></td>
                            <td><a href="#">B008CO812U</a></td>
                            <td><a href="#">http://www.kmart.com/shc/p_1 ...</a></td>
                            <td>8</td>
                            <td>4321</td>
                            <td>Cars</td>
                            <td>$55.00</td>
                            <td>$86.83</td>
                            <td>$18.81</td>
                            <td>25.48%</td>
                            <td>46</td>
                            <td>01/13/2014</td>
                            <td><input type="checkbox"></td>
                        </tr>
                        <tr>
                            <td><a href="#">Discovery Kids Rocketship Projection ...</a></td>
                            <td><a href="#">B00B0M7L22</a></td>
                            <td><a href="#">http://www.amazon.com/Discovery-Kids-Rocketship- ...</a></td>
                            <td>2</td>
                            <td>853</td>
                            <td>Collectibles</td>
                            <td>$59.99</td>
                            <td>$79.99</td>
                            <td>$10.83</td>
                            <td>75.23%</td>
                            <td>14</td>
                            <td>01/12/2015</td>
                            <td><input type="checkbox"></td>
                    </tbody>
                </table>
            </div>                         
        </div>

        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-info pull-right" type="button">Purchase</button>
            </div>
        </div>
    </div>

    <div id="footer">
        <div class="container">
            <p class="text-muted">Place sticky footer content here.</p>
        </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo base_url() ?>assets/js/jquery-1.11.0.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url() ?>assets/js/bootstrap.js"></script>
    <!-- DataTables javascript files -->
    <script src="<?php echo base_url() ?>assets/js/jquery.dataTables.js"></script>

    <script>
    $(document).ready( function () {
        $('.table').dataTable( {
            //"sDom": '<"top">rt<"bottom"flp><"clear">',
            "bLengthChange": false,
            "bFilter": false,
            "aoColumnDefs": [
                { 'bSortable': false, 'aTargets': [ -1 ] }
            ],
            "bPaginate": false
        } );
    } );
    </script>

  </body>
</html>