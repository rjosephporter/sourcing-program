<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sourcing Program - <?php echo $title ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.css" rel="stylesheet">

    <!-- Main Stylesheet -->
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">

    <?php if( isset( $additional_css ) ) echo $additional_css ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="navbar navbar-default navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Sourcing Program</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="<?php echo ($main_segment == FALSE)              ? 'active' : '' ?>"><a href="<?php echo base_url() ?>">Scout</a></li>
            <li class="<?php echo ($main_segment == 'source_products')  ? 'active' : '' ?>"><a href="<?php echo base_url('source_products') ?>">Source Products</a></li>
            <li class="<?php echo ($main_segment == 'buy')              ? 'active' : '' ?>"><a href="<?php echo base_url('buy') ?>">Buy</a></li>
            <li class="<?php echo ($main_segment == 'settings')         ? 'active' : '' ?>"><a href="<?php echo base_url('settings') ?>">Settings</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Other Option <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">
        <?php echo $main_content ?>
    </div>

    <div id="footer">
        <div class="container">
            <p class="text-muted">Place footer content here.</p>
        </div>
    </div>    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo base_url() ?>assets/js/jquery-1.11.0.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url() ?>assets/js/bootstrap.js"></script>
    <script src="<?php echo base_url() ?>assets/js/spin.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.spin.js"></script>

    <?php if( isset( $additional_js ) ) echo $additional_js ?>

  </body>
</html>