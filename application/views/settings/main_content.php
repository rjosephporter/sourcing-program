<div class="row">

	<div class="col-md-8 col-md-offset-2">

		<form class="form-horizontal" role="form">
			<div class="form-group">
				<label for="input-buy-product-row-color" class="col-sm-4 control-label">Buy Product Row Color</label>
				<div class="col-sm-8">
					<div class="color-selector" id="input-buy-product-row-color"><div style="background-color: #0000ff"></div></div>
				</div>
			</div>
			<div class="form-group">
				<label for="input-high-inventory-range" class="col-sm-4 control-label">High Inventory Range</label>
				<div class="col-sm-4">
					<input required  type="number" class="form-control" id="input-high-inventory-min" placeholder="Minimum">
				</div>
				<div class="col-sm-4">
					<input required  type="number" class="form-control" id="input-high-inventory-max" placeholder="Maximum">
				</div>
			</div>
			<div class="form-group">
				<label for="input-high-inventory-row-color" class="col-sm-4 control-label">High Inventory Row Color</label>
				<div class="col-sm-8">
					<div class="color-selector" id="input-high-inventory-row-color"><div style="background-color: #0000ff"></div></div>
				</div>
			</div>
			<div class="form-group">
				<label for="input-low-inventory-range" class="col-sm-4 control-label">Low Inventory Range</label>
				<div class="col-sm-4">
					<input required  type="number" class="form-control" id="input-low-inventory-min" placeholder="Minimum">
				</div>
				<div class="col-sm-4">
					<input required  type="number" class="form-control" id="input-low-inventory-max" placeholder="Maximum">
				</div>
			</div>
			<div class="form-group">
				<label for="input-low-inventory-row-color" class="col-sm-4 control-label">Low Inventory Row Color</label>
				<div class="col-sm-8">
					<div class="color-selector" id="input-low-inventory-row-color"><div style="background-color: #0000ff"></div></div>
				</div>
			</div>
			<div class="form-group">
				<label for="input-purchased-row-color" class="col-sm-4 control-label">Purchased Product Row Color</label>
				<div class="col-sm-8">
					<div class="color-selector" id="input-purchased-row-color"><div style="background-color: #0000ff"></div></div>
				</div>
			</div>
			<div class="form-group">
				<label for="input-purchased-highlight-days" class="col-sm-4 control-label">Purchased Product Highlight Days</label>
				<div class="col-sm-8">
					<input required  type="number" class="form-control" id="input-purchased-highlight-days" placeholder="Enter number fof days">
				</div>
			</div>
			<!--
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<div class="checkbox">
						<label>
							<input type="checkbox"> Remember me
						</label>
					</div>
				</div>
			</div>
			-->
			<div class="form-group">
				<div class="col-sm-offset-4 col-sm-8">
					<button type="submit" class="btn btn-info">Apply</button>
				</div>
			</div>
		</form>

	</div>

</div>