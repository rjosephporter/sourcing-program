<div class="row">
    <div class="col-md-12">
        <div class="input-group">
            <input id="input-search" type="search" class="form-control input-lg" placeholder="Search product by name or ASIN">
            <span class="input-group-btn">
                <button id="button-search" class="btn btn-info btn-lg" type="button">
                    <span class="glyphicon glyphicon-search"></span>
                </button>
            </span>
        </div><!-- /input-group -->
    </div>
</div>
<div class="search-results" id="search-results">
    <div id="search-results-body">
        <div class="row error-msg" style="display: none;">
            <div class="col-md-12"><h5>No results found.</h5></div>
        </div>
        <div class="row">
            <div class="col-md-12 source-product-results table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>ASIN</th>
                            <th>Source Link</th>
                            <th>Source Inventory</th>
                            <th>Best Seller Rank</th>
                            <th>Category</th>
                            <th>Store Price</th>
                            <th>Amazon Price</th>
                            <th>Net Profit</th>
                            <th>ROI</th>
                            <th>Number of Sellers</th>
                            <th>Last Ordered</th>
                            <th>Buy Product</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><a href="#">EB Brands Remote Controlled Ex ...</a></td>
                            <td><a href="#">B00EH8VDWK</a></td>
                            <td><a href="#">http://www.walmart.com/ip/Remo ...</a></td>
                            <td>5</td>
                            <td>234567</td>
                            <td>Toys</td>
                            <td>$69.88</td>
                            <td>$109.93</td>
                            <td>$23.56</td>
                            <td>25.21%</td>
                            <td>35</td>
                            <td>12/31/2013</td>
                            <td><input type="checkbox"></td>
                        </tr>
                        <tr>
                            <td><a href="#">Pink Whisper Ride Buggy ...</a></td>
                            <td><a href="#">B008CO812U</a></td>
                            <td><a href="#">http://www.kmart.com/shc/p_1 ...</a></td>
                            <td>8</td>
                            <td>4321</td>
                            <td>Cars</td>
                            <td>$55.00</td>
                            <td>$86.83</td>
                            <td>$18.81</td>
                            <td>25.48%</td>
                            <td>46</td>
                            <td>01/13/2014</td>
                            <td><input type="checkbox"></td>
                        </tr>
                        <tr>
                            <td><a href="#">Discovery Kids Rocketship Projection ...</a></td>
                            <td><a href="#">B00B0M7L22</a></td>
                            <td><a href="#">http://www.amazon.com/Discovery-Kids-Rocketship- ...</a></td>
                            <td>2</td>
                            <td>853</td>
                            <td>Collectibles</td>
                            <td>$59.99</td>
                            <td>$79.99</td>
                            <td>$10.83</td>
                            <td>75.23%</td>
                            <td>14</td>
                            <td>01/12/2015</td>
                            <td><input type="checkbox"></td>
                    </tbody>
                </table>
            </div>                         
        </div>
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-info pull-right" type="button">Add Products</button>
            </div>
        </div>
    </div>
    



</div>

