<?php foreach ( $products as $asin => $details): ?>
<div class="col-md-12" id="product-info-small-<?php echo $asin ?>">
    <div class="row">
        <div class="col-md-4 product-image-small"><img src="<?php echo $details['Image_URL'] ?>" class="img-responsive" /></div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <h5><?php echo $details['Title'] ?></h5>
                </div>
            </div>
            <div class="product-attr">
                <div class="row">
                    <div class="col-md-5 text-right product-attr-title"><strong>Category:</strong></div>
                    <div class="col-md-7 product-attr-value"><?php echo $details['Category'] ?></div>
                </div>
                <div class="row">
                    <div class="col-md-5 text-right product-attr-title"><strong>Salesrank:</strong></div>
                    <div class="col-md-7 product-attr-value"><?php echo $details['Salesrank'] ?></div>
                </div>
                <div class="row">
                    <div class="col-md-5 text-right product-attr-title"><strong>Lowest Offer:</strong></div>
                    <div class="col-md-7 product-attr-value"><?php echo (isset($details['Lowest_Offer'])) ? $details['Lowest_Offer'] : 'N/A' ?></div>
                </div>
                <div class="row">
                    <div class="col-md-5 text-right product-attr-title"><strong>Total Offers:</strong></div>
                    <div class="col-md-7 product-attr-value"><?php echo $details['Total_Offers'] ?></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12"><button asin="<?php echo $asin ?>" class="btn btn-info btn-block btn-get-product-offers" type="button">Get Product Offers</button></div>
    </div>
    <hr>
</div>
<?php endforeach; ?>