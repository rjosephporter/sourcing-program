<div class="row">
    <div class="col-md-12">
        <div class="input-group">
            <input id="input-search" type="search" class="form-control input-lg" placeholder="Search Amazon for product's Name, ASIN, or UPC">
            <span class="input-group-btn">
                <button id="button-search" class="btn btn-info btn-lg" type="button">
                    <span class="glyphicon glyphicon-search"></span>
                </button>
            </span>
        </div><!-- /input-group -->
    </div>
</div>

<div class="panel panel-primary search-results" id="search-results" style="display: none;">
    <div class="panel-heading">
        <h3 class="panel-title">Search Results</h3>
    </div>
    <div class="panel-body" id="search-results-body">
        <p class="error-msg" style="display: none;">No results found.</p>
        <div class="row">
            <div class="col-md-4" id="product-info-results-small">
                <!-- Product Info Results - Left Side -->
            </div>
            <div class="col-md-8" id="product-info-big-container">
                <div id="product-info-big">
                    <!-- Product Info Results - Right Side -->    
                </div>               
            </div>
        </div>
    </div>
</div>