<div class="row">
    <div class="col-md-4 product-image-small"><img src="<?php echo $detail['Image_URL'] ?>" class="img-responsive" /></div>
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-12">
                <h4><?php echo $detail['Title'] ?></h4>
            </div>
        </div>
        <div class="col-md-6 product-attr">
            <div class="row">
                <div class="col-md-5 text-right product-attr-title"><strong>Category:</strong></div>
                <div class="col-md-7 product-attr-value"><?php echo $detail['Category'] ?></div>
            </div>
            <div class="row">
                <div class="col-md-5 text-right product-attr-title"><strong>Salesrank:</strong></div>
                <div class="col-md-7 product-attr-value"><?php echo $detail['Salesrank'] ?></div>
            </div>
            <div class="row">
                <div class="col-md-5 text-right product-attr-title"><strong>Weight:</strong></div>
                <div class="col-md-7 product-attr-value"><?php echo $detail['Weight'] ?></div>
            </div>
            <div class="row">
                <div class="col-md-5 text-right product-attr-title"><strong>ASIN:</strong></div>
                <div class="col-md-7 product-attr-value"><?php echo $asin ?></div>
            </div>
            <div class="row">
                <div class="col-md-12 product-attr-value">&nbsp;</div>
            </div>
            <div class="row">
                <div class="col-md-12 product-attr-value"><a href="http://www.amazon.com/dp/<?php echo $asin ?>" target="_blank">Go to Amazon page</a></div>
            </div>
        </div>
        <div class="col-md-6 product-attr">
            <div class="row">
                <div class="col-md-5 text-right product-attr-title"><strong>List Price:</strong></div>
                <div class="col-md-7 product-attr-value"><?php echo $detail['List_Price'] ?></div>
            </div>
            <div class="row">
                <div class="col-md-5 text-right product-attr-title"><strong>Lowest Offer:</strong></div>
                <div class="col-md-7 product-attr-value"><?php echo isset($detail['Lowest_Offer']) ? $detail['Lowest_Offer'] : 'N/A' ?></div>
            </div>
            <div class="row">
                <div class="col-md-5 text-right product-attr-title"><strong>Total Offers:</strong></div>
                <div class="col-md-7 product-attr-value"><?php echo $detail['Total_Offers'] ?></div>
            </div>
            <div class="row">
                <div class="col-md-5 text-right product-attr-title"><strong>Size Group:</strong></div>
                <div class="col-md-7 product-attr-value"><?php echo $detail['SizeGroup'] ?></div>
            </div>
            <div class="row">
                <div class="col-md-5 text-right product-attr-title"><strong>Size Tier:</strong></div>
                <div class="col-md-7 product-attr-value"><?php echo $detail['SizeTier'] ?></div>
            </div>
        </div>
    </div>
</div>  
<hr>

<div class="row">

    <div class="col-md-12">
        <form class="form-inline" role="form">
            <div class="form-group">
                <label for="desired_price">Desired Price:&nbsp;&nbsp;&nbsp;</label>
                <div class="form-group">
                    <div class="input-group" style="width: 200px">
                      <span class="input-group-addon">$</span>
                      <input type="text" class="form-control" id="desired_price">
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-default">Calculate</button>
        </form>
    </div>

</div>

<div class="row">
    <div class="col-md-6">
        <table class="table table-striped table-condensed">
            <thead>
                <tr>
                    <th>FBA</th>
                    <th colspan="2" class="payout-title text-center">Payout</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>NEW</td>
                    <td>FBA</td>
                    <td>MF</td>
                </tr>
                <?php foreach($fulfillment_fba['new'] as $value): ?>
                    <tr>
                        <td><?php echo $value ?></td>
                        <td class="payout-value">XX.XX USD</td>
                        <td class="payout-value">XX.XX USD</td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table> 
    </div>
    <div class="col-md-6">
        <table class="table table-striped table-condensed">
            <thead>
                <tr>
                    <th>Merchant</th>
                    <th colspan="2" class="payout-title text-center">Payout</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>NEW</td>
                    <td>FBA</td>
                    <td>MF</td>
                </tr>
                <?php foreach($fulfillment_merchant['new'] as $value): ?>
                    <tr>
                        <td><?php echo $value ?></td>
                        <td class="payout-value">XX.XX USD</td>
                        <td class="payout-value">XX.XX USD</td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table> 
    </div>                           
</div> 