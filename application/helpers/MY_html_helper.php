<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Extension of the core HTML helper
 *
 * @author rjosephporter
 *
 */

// ------------------------------------------------------------------------

/**
 * JS
 *
 * Generates a script tag for calling external javascript file.
 *
 * @access	public
 * @param	mixed 			a string or array of strings containing the filename of the javascript file
 * @param	string 			base path 
 * @return	string
 * @author 	rjosephporter
 */
if ( ! function_exists('js'))
{
	function js($files, $js_path = 'assets/js/')
	{
		$result = '';

		if ( is_array($files) ) {
			foreach ( $files as $file ) {
				$result .= "<script src='".base_url($js_path.$file)."'></script>";
			}
		} else {
			$result = "<script src='".base_url($js_path.$files)."'></script>";
		}

		return $result;
	}
}

// ------------------------------------------------------------------------

/**
 * css
 *
 * Generates a link tag for calling external CSS file.
 *
 * @access	public
 * @param	mixed 			a string or array of strings containing the filename of the CSS file
 * @param	string 			base path 
 * @return	string
 * @author 	rjosephporter
 */
if ( ! function_exists('css'))
{
	function css($files, $css_path = 'assets/css/')
	{
		$result = '';

		if ( is_array($files) ) {
			foreach ( $files as $file ) {
				$result .= "<link href='".base_url($css_path.$file)."' rel='stylesheet'>";
			}
		} else {
			$result = "<link href='".base_url($css_path.$files)."' rel='stylesheet'>";
		}

		return $result;
	}
}

// ------------------------------------------------------------------------